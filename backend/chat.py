import logging
import rsa

from socket import *
from enum import Enum
from ipaddress import IPv4Network
from time import time, sleep
from rsa import DecryptionError
from threading import Thread

class _Message(Enum):
    HELLO = 'HELLO'
    HELLO_REPLY = 'HELLO_REPLY'
    BYE = 'BYE'
    KEY = 'KEY'
    MESSAGE = 'MESSAGE'

def get_my_ip():
    s = socket(AF_INET, SOCK_DGRAM)
    s.settimeout(0)
    try:
        # doesn't even have to be reachable
        s.connect(('10.254.254.254', 1))
        ip = s.getsockname()[0]
    except Exception:
        ip = '127.0.0.1'
    finally:
        s.close()
    return ip


class Chat:
    def __init__(self, messaging_port: int, broadcast_port: int, mask_bits: int, username: str, max_connections=5):
        self.messaging_port = messaging_port
        self.broadcast_port = broadcast_port
        self.mask_bits = mask_bits
        self.username = username
        self.max_connections = max_connections

        self.encryption_bits = 2048
        self.encoding = 'utf8'
        self.broadcast_buff_size = 4096
        self.message_buff_size = 8192

        self.address = get_my_ip()

        self.connection_listener = socket(AF_INET, SOCK_STREAM)
        self.connection_listener.bind((self.address, self.messaging_port))

        self.broadcast_listener = socket(AF_INET, SOCK_DGRAM)
        self.broadcast_listener.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
        self.broadcast_listener.bind(('', self.broadcast_port))

        self.sender = socket(AF_INET, SOCK_DGRAM)
        self.sender.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)

        self.known_clients = {}
        self.connections = {}
        self.connection_threads = []

        self.running = False
        self.broadcast_thread = None
        self.connections_thread = None
        self.rsa_keys = None


    def start(self):
        logging.info('Generating RSA keys...')
        start_time = time()
        self.rsa_keys = rsa.newkeys(self.encryption_bits)
        logging.info(f'Generating RSA keys took {time() - start_time} seconds')

        self.say_broadcast(f'{_Message.HELLO.value} {self.username}')
        self.running = True
        self.broadcast_thread = Thread(target=self.listen_broadcast)
        self.broadcast_thread.start()
        self.connections_thread = Thread(target=self.listen_for_connections)
        self.connections_thread.start()
        logging.info('Successfully started')

    def stop(self):
        for active_connection in self.connections.items():
            active_connection[1]['socket'].send(f'{_Message.BYE.value}  '.encode(self.encoding))
        sleep(0.5)
        self.running = False
        self.say_broadcast(_Message.BYE.value)
        try:
            self.broadcast_listener.shutdown(SHUT_RDWR)
        except Exception:
            self.broadcast_listener.close()
        try:
            self.connection_listener.shutdown(SHUT_RDWR)
        except Exception:
            self.connection_listener.close()
        self.broadcast_thread.join()
        self.connections_thread.join()
        for connection in self.connections.items():
            connection[1]['socket'].close()
        for connection_thread in self.connection_threads:
            connection_thread.join()
        logging.info('Successfully stopped')

    def say_broadcast(self, message: str):
        self.sender.sendto(message.encode(self.encoding), self.__broadcast_address())

    def listen_for_connections(self):
        self.connection_listener.listen(self.max_connections)
        while self.running:
            socket_connection, address = self.connection_listener.accept()

            if address[0] in self.connections:
                continue

            self.connections[address[0]] = {
                'socket': socket_connection,
                'username': self.known_clients[address[0]] if address[0] in self.known_clients else address[0],
                'messages': []
            }
            logging.info(f'New connection with {address}')
            conn_thread = Thread(target=self.__handle_user_connection, args=[self.connections[address[0]], address[0]])
            self.connection_threads.append(conn_thread)
            conn_thread.start()

    def listen_broadcast(self):
        while self.running:
            try:
                message_and_address = self.broadcast_listener.recvfrom(self.broadcast_buff_size)
            except:
                break
            ip = message_and_address[1][0]
            if ip == self.address:
                continue

            message = message_and_address[0].decode(self.encoding).split(' ')
            if message is None or len(message) == 0:
                logging.warning(f'Received empty message from {ip}')
                continue

            match message[0]:
                case _Message.HELLO.value:
                    if len(message) < 2:
                        logging.warning(f'Incorrect message "{message[0]}" from {ip}')
                        continue
                    self.__add_user(ip, ' '.join([message[i] for i in range(1, len(message))]))
                    self.broadcast_listener.sendto(f'{_Message.HELLO_REPLY.value} {self.username}'.encode(self.encoding),
                                                   (ip, self.broadcast_port))
                case _Message.HELLO_REPLY.value:
                    if len(message) < 2:
                        logging.warning(f'Incorrect message "{message[0]}" from {ip}')
                        continue
                    self.__add_user(ip, ' '.join([message[i] for i in range(1, len(message))]))
                case _Message.BYE.value:
                    if ip in self.known_clients:
                        del self.known_clients[ip]
                        logging.info(f'User {ip} has left')
                        if ip in self.connections:
                            self.__close_connection(self.connections[ip], ip)

    def get_messages_by_ip(self, ip: str):
        if ip not in self.connections:
            return []
        try:
            return self.connections[ip]['messages']
        except Exception:
            return []

    def send_message_to_ip(self, ip: str, message: str) -> str:
        if ip not in self.connections:
            raise ValueError(f'{ip} is not in connections')
        if 'public_key' not in self.connections[ip]:
            raise ValueError(f'User {ip} does not send his public key -> unable to send message')
        self.connections[ip]['socket'].send(f'{_Message.MESSAGE.value} '.encode(self.encoding) +
                                            rsa.encrypt(
                                                message.encode(self.encoding),
                                                self.connections[ip]['public_key']
                                            ))
        self.connections[ip]['messages'].append(f'You: {message}')
        return self.connections[ip]['messages'][-1]

    def connect(self, ip):
        if ip in self.connections:
            return

        if len(self.connections) >= self.max_connections:
            logging.warning('Unable to create new connection: exceeded maximum number of connections')
            return

        new_connection = {
            'socket': socket(AF_INET, SOCK_STREAM),
            'username': self.known_clients[ip] if ip in self.known_clients else ip,
            'messages': []
        }
        new_connection['socket'].connect((ip, self.messaging_port))
        logging.info(f'New connection with {ip}')
        self.connections[ip] = new_connection
        conn_thread = Thread(target=self.__handle_user_connection, args=[self.connections[ip], ip])
        self.connection_threads.append(conn_thread)
        conn_thread.start()

    def __handle_user_connection(self, connection, ip):
        connection['socket'].send(f'{_Message.KEY.value} {self.rsa_keys[0].n} {self.rsa_keys[0].e}'.encode(self.encoding))
        while self.running:
            try:
                message_bytes = connection['socket'].recv(self.message_buff_size)

                if not message_bytes:
                    logging.warning(f'Failed to decode message from {ip} {connection["username"]}')

                try:
                    first_space_index = message_bytes.index(' '.encode(self.encoding))
                except Exception:
                    logging.warning(f'Incorrect message from {ip}')
                    continue

                command = message_bytes[:first_space_index].decode(self.encoding)
                message = message_bytes[(first_space_index + 1):]

                if 'public_key' not in connection and command != _Message.KEY.value:
                    logging.warning(f'User {ip} {connection["username"]} is trying to send message without sending public key')
                    continue

                match command:
                    case _Message.KEY.value:
                        message = message.decode(self.encoding).split(' ')
                        if len(message) < 2:
                            logging.warning(f'Incorrect message from {ip} {connection["username"]}: {" ".join(message)}')
                            continue
                        connection['public_key'] = rsa.PublicKey(int(message[0]), int(message[1]))
                    case _Message.MESSAGE.value:
                        connection['messages'].append(f'{connection["username"]}: ' + rsa.decrypt(message, self.rsa_keys[1]).decode(self.encoding))
                        logging.info(f'{ip} {connection["username"]} - {connection["messages"][-1]}')
                    case _Message.BYE.value:
                        connection['messages'].append(f'User {connection["username"]} left the chat')
                        sleep(1)
                        break
            except DecryptionError as e:
                logging.error(f'Error to decode message: {e}')
            except Exception as e:
                logging.error(f'Error to handle user connection: {e}')
                self.__close_connection(connection, ip)
                break
        self.__close_connection(connection, ip)

    def __add_user(self, ip, username):
        if ip not in self.known_clients:
            self.known_clients[ip] = username
            logging.info(f'New known user: {ip}')

    def __close_connection(self, conn, ip):
        conn['socket'].close()
        if ip in self.connections:
            del self.connections[ip]

    def __broadcast_address(self):
        return (str(IPv4Network(f'{self.address}/{self.mask_bits}', False).broadcast_address),
                self.broadcast_port)
