import logging
import sys
from os import getenv

from PyQt6.QtWidgets import QApplication, QInputDialog, QLineEdit
from dotenv import load_dotenv

from ui import Window

if __name__ == '__main__':
    logging.basicConfig(level='INFO')
    app = QApplication(sys.argv)
    load_dotenv()
    screen = Window(app.primaryScreen().size(), int(getenv('CHAT_MESSAGING_PORT')), int(getenv('CHAT_BROADCAST_PORT')),
                    int(getenv('CHAT_ADDRESS_MASK')))
    text, ok_pressed = QInputDialog.getText(screen, 'Login', 'Enter your nickname:', QLineEdit.EchoMode.Normal, "")
    if text is None or len(text) == 0 or not ok_pressed:
        exit()
    screen.start_chat(text)
    screen.show()
    sys.exit(app.exec())
