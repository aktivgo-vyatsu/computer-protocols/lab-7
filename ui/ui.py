from threading import Thread

from PyQt6.QtCore import Qt
from PyQt6.QtWidgets import QWidget, QGridLayout, QListWidget, QPushButton, QLabel, QTextEdit, QLineEdit, QMessageBox

from backend import Chat


class Window(QWidget):
    def __init__(self, screen_size, messaging_port: int, broadcast_port: int, mask_bits: int):
        QWidget.__init__(self)
        self.messaging_port = messaging_port
        self.broadcast_port = broadcast_port
        self.mask_bits = mask_bits

        self.setFixedWidth(int(screen_size.width() / 1.5))
        self.setFixedHeight(int(screen_size.height() / 1.5))
        self.setWindowTitle('Chat')

        layout = QGridLayout()
        self.setLayout(layout)

        layout.setColumnStretch(0, 1)
        layout.setColumnStretch(1, 3)

        self.users_label = QLabel('User list')
        layout.addWidget(self.users_label, 0, 0, 1, 1, Qt.AlignmentFlag.AlignCenter)

        self.users = QListWidget()
        self.users.itemDoubleClicked.connect(self.connect_with)
        layout.addWidget(self.users, 1, 0, 1, 1)

        self.info = QLabel()
        layout.addWidget(self.info, 0, 1, 1, 1)

        self.text_edit_layout = QGridLayout()

        self.text_edit = QTextEdit()
        self.text_edit.setReadOnly(True)
        self.text_edit_layout.addWidget(self.text_edit, 0, 0, 1, 2)

        self.message_send = QLineEdit()
        self.text_edit_layout.addWidget(self.message_send, 1, 0)

        self.send_button = QPushButton('Send')
        self.send_button.clicked.connect(self.send_message)
        self.text_edit_layout.addWidget(self.send_button, 1, 1)

        layout.addLayout(self.text_edit_layout, 1, 1, 1, 1)

        self.chat = None
        self.current_connection = None
        self.check_new_clients = True
        self.check_new_messages = True
        self.messages_count = 0
        self.last_message_count = 0
        self.update_checker_thread = None

    def start_chat(self, username):
        self.chat = Chat(self.messaging_port, self.broadcast_port, self.mask_bits, username)
        self.chat.start()
        self.update_checker_thread = Thread(target=self.check_for_updates)
        self.update_checker_thread.start()

    def check_for_updates(self):
        while self.chat.running:
            if self.check_new_clients:
                if len(self.chat.known_clients) != self.users.count():
                    self.users.clear()
                    for client in self.chat.known_clients.items():
                        self.users.addItem(f'{client[1]} - {client[0]}')
            if self.check_new_messages and self.current_connection is not None:
                messages = self.chat.get_messages_by_ip(self.current_connection[1])
                if len(messages) - self.last_message_count < 0:
                    self.messages_count = 0
                self.last_message_count = len(messages)
                messages_count_diff = len(messages) - self.messages_count
                if messages_count_diff > 0:
                    for message in messages[-messages_count_diff:]:
                        self.text_edit.append(message)
                        self.messages_count += 1

    def connect_with(self, item):
        self.check_new_messages = False
        self.text_edit.clear()
        self.current_connection = item.text().split(' - ')
        self.chat.connect(self.current_connection[1])
        self.info.setText(item.text())

        self.messages_count = 0
        for message in self.chat.get_messages_by_ip(self.current_connection[1]):
            self.text_edit.append(message)
            self.messages_count += 1
        self.check_new_messages = True

        msg = QMessageBox()
        msg.setIcon(QMessageBox.Icon.Information)
        msg.setWindowTitle('Connection')
        msg.setText(f'You have successfully connected to the chat with {self.current_connection[0]}')
        msg.exec()

    def send_message(self):
        if self.current_connection is None:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Icon.Warning)
            msg.setWindowTitle('Sending error')
            msg.setText('You have not connected to any user')
            msg.exec()
            return

        self.check_new_messages = False
        try:
            sent_message = self.chat.send_message_to_ip(self.current_connection[1], self.message_send.text())
            self.text_edit.append(sent_message)
            self.messages_count += 1
            self.message_send.clear()
            self.check_new_messages = True
        except Exception:
            self.check_new_messages = True
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Icon.Warning)
            msg.setWindowTitle('Sending error')
            msg.setText('Failed to send message')
            msg.exec()

    def closeEvent(self, event):
        self.chat.stop()
        if self.update_checker_thread is not None:
            self.update_checker_thread.join()

    def keyPressEvent(self, QKeyEvent):
        if QKeyEvent.key() == Qt.Key.Key_Return:
            self.send_message()
